groups:
- name: GitLab Component Operations-per-Second Rates
  interval: 1m
  rules:
  # Postgres Service
  - record: gitlab_component_ops:rate
    labels:
      component: 'service'
      stage: 'main'
    expr: >
      sum by (environment, tier, type) (rate(pg_stat_database_xact_commit{type="patroni", tier="db"}[1m]))
      +
      sum by (environment, tier, type) (rate(pg_stat_database_xact_rollback{type="patroni", tier="db"}[1m]))

  # Redis Service
  - record: gitlab_component_ops:rate
    labels:
      component: 'server'
      stage: 'main'
    expr: >
      sum by (environment, tier, type) (redis:commands_processed:irate1m)

  # Registry Service
  - record: gitlab_component_ops:rate
    labels:
      type: 'registry'
      component: 'server'
      tier: 'sv'
      stage: 'main'
    expr: >
      sum by (environment) (rate(haproxy_backend_http_responses_total{backend="registry"}[1m]))

  - record: gitlab_component_ops:rate
    labels:
      type: 'registry'
      component: 'server'
      tier: 'sv'
      stage: 'cny'
    expr: >
      sum by (environment) (rate(haproxy_backend_http_responses_total{backend="canary_registry"}[1m]))

  # gitlab_shell
  - record: gitlab_component_ops:rate
    labels:
      tier: 'sv'
      type: 'git'
      component: 'gitlab_shell'
      stage: 'main'
    expr: >
      sum by (environment) (haproxy_backend_current_session_rate{backend=~"ssh|altssh"})

  # web:workhorse
  - record: gitlab_component_ops:rate
    labels:
      component: 'workhorse'
    expr: >
      sum by (environment, tier, type, stage) (rate(gitlab_workhorse_http_requests_total{job="gitlab-workhorse-web"}[1m]))

  # api:workhorse
  - record: gitlab_component_ops:rate
    labels:
      component: 'workhorse'
    expr: >
      sum by (environment, tier, type, stage) (rate(gitlab_workhorse_http_requests_total{job="gitlab-workhorse-api"}[1m]))

  # api/web/sidekiq/git:unicorn
  - record: gitlab_component_ops:rate
    labels:
      component: 'unicorn'
    expr: >
      sum by (environment, tier, type, stage) (rate(http_request_duration_seconds_count{job=~"gitlab-(rails|unicorn)"}[1m]))

  # sidekiq: job RPS rate
  - record: gitlab_component_ops:rate
    labels:
      component: 'jobs'
    expr: >
      sum by (environment, tier, type, stage) (rate(sidekiq_jobs_completion_time_seconds_count{type = "sidekiq"}[1m]))

  # mailroom
  # TODO: zero placeholder as we have no metrics for mailroom at present
  - record: gitlab_component_ops:rate
    labels:
      component: 'service'
    expr: >
      count by (environment, tier, type, stage) (up{type="mailroom"}) - count by (environment, tier, type) (up{type="mailroom"})

  # pgbouncer
  - record: gitlab_component_ops:rate
    labels:
      component: 'service'
      stage: main
    expr: >
      sum by (environment, tier, type) (rate(pgbouncer_stats_queries_total[1m]))

  # gitaly:goserver
  - record: gitlab_component_ops:rate
    labels:
      component: 'goserver'
    expr: >
      sum by (environment, tier, type, stage) (rate(grpc_server_started_total{type="gitaly"}[1m]))

  # pages - haproxy service in front of GitLab pages
  - record: gitlab_component_ops:rate
    labels:
      component: 'haproxy_http'
    expr: >
      sum (rate(haproxy_backend_http_responses_total{type="pages"}[1m])) by (environment, type, tier, stage)

  - record: gitlab_component_ops:rate
    labels:
      component: 'haproxy_https'
    expr: >
      sum(haproxy_backend_current_session_rate{backend="pages_https", type="pages"}) by (environment, type, tier, stage)

  # web-pages
  - record: gitlab_component_ops:rate
    labels:
      component: 'http'
    expr: >
      sum(rate(gitlab_pages_http_requests_total{}[1m])) by (environment, tier, type, stage)

  # HAProxy
  - record: gitlab_component_ops:rate
    labels:
      type: 'frontend'
      stage: 'main'
    expr: >
      sum(rate(haproxy_backend_http_responses_total{backend!~"canary_.*"}[1m])) by (environment, tier)

  - record: gitlab_component_ops:rate
    labels:
      type: 'frontend'
      stage: 'cny'
    expr: >
      sum(rate(haproxy_backend_http_responses_total{backend=~"canary_.*"}[1m])) by (environment, tier)

  # monitoring: thanos_instant_query
  - record: gitlab_component_ops:rate
    labels:
      component: 'thanos_instant_query'
    expr: >
      sum(rate(thanos_query_api_instant_query_duration_seconds_count{type="monitoring"}[1m])) by (environment, tier, type, stage)

  # monitoring: thanos_range_query
  - record: gitlab_component_ops:rate
    labels:
      component: 'thanos_range_query'
    expr: >
      sum(rate(thanos_query_api_range_query_duration_seconds_count{type="monitoring"}[1m])) by (environment, tier, type, stage)

  # monitoring: prometheus http request
  - record: gitlab_component_ops:rate
    labels:
      component: 'prometheus_http'
    expr: >
      sum(rate(prometheus_http_request_duration_seconds_count{type="monitoring"}[1m])) by (environment, tier, type, stage)

  # Stackdriver
  - record: gitlab_component_ops:rate
    labels:
      type: stackdriver
      tier: inf
      stage: main
    expr: >
      sum(
        label_replace(
          stackdriver_gce_instance_logging_googleapis_com_log_entry_count, "component", "$1", "log", "(.*)"
        )
      ) by (environment, component)

  # ci-runners
  - record: gitlab_component_ops:rate
    labels:
      type: ci-runners
      component: 'polling'
      tier: runners
    # See https://gitlab.com/gitlab-org/gitlab-workhorse/blob/master/internal/builds/register.go for details of each status label
    expr: >
      sum by (environment, stage) (rate(gitlab_workhorse_builds_register_handler_requests[1m]))

  # monitoring:thanos
  - record: gitlab_component_errors:rate
    labels:
      component: 'thanos'
    expr: >
      sum(rate(http_requests_total{job="thanos"}[1m])) by (environment, tier, type, stage)

  # monitoring:prometheus
  - record: gitlab_component_errors:rate
    labels:
      component: 'prometheus'
    expr: >
      sum(rate(prometheus_http_requests_total{job="prometheus"}[1m])) by (environment, tier, type, stage)

- name: GitLab Service Operations-per-Second Aggregated Rates
  interval: 1m
  rules:
  # Aggregate over all components within a service
  - record: gitlab_service_ops:rate
    expr: >
      sum by (environment, tier, type, stage) (gitlab_component_ops:rate >= 0)

- name: GitLab Component Operations-per-Second Rate Stats
  interval: 5m
  rules:
  # Average values for each component, over a week
  - record: gitlab_component_ops:rate:avg_over_time_1w
    expr: >
      avg_over_time(gitlab_component_ops:rate[1w])
  # Stddev for each component, over a week
  - record: gitlab_component_ops:rate:stddev_over_time_1w
    expr: >
      stddev_over_time(gitlab_component_ops:rate[1w])

- name: GitLab Service Operations-per-Second Rate Stats
  interval: 5m
  rules:
  # Average values for each service, over a week
  - record: gitlab_service_ops:rate:avg_over_time_1w
    expr: >
      avg_over_time(gitlab_service_ops:rate[1w])
  # Stddev for each service, over a week
  - record: gitlab_service_ops:rate:stddev_over_time_1w
    expr: >
      stddev_over_time(gitlab_service_ops:rate[1w])

- name: GitLab Service Ops Rate Weekly Periodic Values
  interval: 5m
  rules:
  # Predict what the value should be using the median value for a
  # four hour period, for the past 3 weeks include week-on-week growth...
  - record: gitlab_service_ops:rate:prediction
    expr: >
      quantile(0.5,
        label_replace(
          avg_over_time(
            gitlab_service_ops:rate[4h] offset 166h # 1 week - 2 hours
          )
          + gitlab_service_ops:rate:avg_over_time_1w - gitlab_service_ops:rate:avg_over_time_1w offset 1w
          , "p", "1w", "", "")
        or
        label_replace(
          avg_over_time(
            gitlab_service_ops:rate[4h] offset 334h # 2 weeks - 2 hours
          )
          + gitlab_service_ops:rate:avg_over_time_1w - gitlab_service_ops:rate:avg_over_time_1w offset 2w
          , "p", "2w", "", "")
        or
        label_replace(
          avg_over_time(
            gitlab_service_ops:rate[4h] offset 502h # 3 weeks - 2 hours
          )
          + gitlab_service_ops:rate:avg_over_time_1w - gitlab_service_ops:rate:avg_over_time_1w offset 3w
          , "p", "3w", "", "")
      )
      without (p)
